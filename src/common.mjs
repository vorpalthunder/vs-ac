export const CFG = {
	id: 'pf1-vs-ac',
	SETTINGS: {
		rangeConfig: 'rangeConfig',
		tutorial: 'tutorial',
		noSaves: 'noSaves',
		showTargets: 'showTargets',
		transparency: 'transparency',
	},
	cache: {
		oldMessages: []
	},
	debug: {
		basic: false,
		math: false,
		chat: false,
		verbose: false,
	},
	COLORS: {
		main: 'color:orangered',
		label: 'color:mediumseagreen',
		id: 'color:darkseagreen',
		number: 'color:mediumpurple',
		unset: 'color:unset'
	}
};
