// import { CFG } from './common.mjs';

/**
 * @param {TokenDocument} doc
 * @param {Object} options
 * @param {Boolean} options.pan Pan to token
 * @param {Boolean} options.select Select token (and release any other previously selected tokens)
 * @param {Boolean} options.swapScene Swap scene to where the token is
 */
export async function focusToken(doc, { pan = true, select = true, swapScene = true, releaseOthers = true } = {}) {
	// const focusCfg = game.settings.get(CFG.module, CFG.SETTINGS.focusKey);

	// Swap scene
	// if (swapScene) await doc.parent?.view();

	// If wrong scene, .object will be null
	const token = doc?.object;
	if (!token) return; // Bad data

	if (!token.isVisible) return; // Don't deal with invisible tokens

	// Center token
	if (pan) canvas.animatePan({ x: token.center.x, y: token.center.y, duration: 250 });
	// Take control of it
	if (select) token.control({ releaseOthers });
}
