import { CFG } from './common.mjs';
import { RangeConfigData } from './range-config-data.mjs';

export class RangeConfigDialog extends FormApplication {
	settings;
	constructor(...args) {
		super(...args);
		this.settings = new RangeConfigData();
		const userSettings = game.settings.get(CFG.id, CFG.SETTINGS.rangeConfig);
		Object.assign(this.settings, userSettings);
	}

	get template() {
		return `modules/${CFG.id}/template/range-config.hbs`;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			title: 'Range Config',
			classes: ['vs-ac', 'vs-ac-config'],
		});
	}

	getData(...args) {
		const data = super.getData(...args);
		data.settings = this.settings;
		return data;
	}

	_updateObject(ev, data) {
		this.settings = new RangeConfigData();
		Object.assign(this.settings, data);
		game.settings.set(CFG.id, CFG.SETTINGS.rangeConfig, this.settings);
	}

	activateListeners(jq) {
		super.activateListeners(jq);
		jq.find('input[type="reset"]').on('click', (ev) => {
			ev.preventDefault();
			this.settings = new RangeConfigData();
			this.render(true);
		});
	}
}
