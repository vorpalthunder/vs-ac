import { CFG } from './common.mjs';
import { sceneTokenDistance } from './math.mjs';
import { focusToken } from './utility.mjs';
import { RangeConfigDialog } from './range-config.mjs';

const sceneScaleTransform = (x, scene) => x / scene.grid.size * scene.grid.distance;
const signNum = (value) => value >= 0 ? `+${value}` : `${value}`;

const setTooltip = (el, label) => {
	el.dataset.tooltip = label;
	el.dataset.tooltipDirection = 'UP';
}

const createNode = (node, text = null, classes = []) => {
	const n = document.createElement(node);
	if (text) n.textContent = text;
	if (classes.length) n.classList.add(...classes);
	return n;
}

const getRollInfo = (rollData, targetAC) => {
	const roll = Roll.fromData(rollData);
	const naturalRoll = roll?.dice[0]?.total; // Assume first term is the die roll
	if (naturalRoll === undefined) return; // Bad data

	const nat1 = naturalRoll === 1,
		nat20 = naturalRoll === 20,
		attackRoll = roll.total ?? 0,
		diff = attackRoll - targetAC; // attack AC diff

	return {
		roll,
		nat1,
		nat20,
		diff,
		success: nat20 || !nat1 && diff >= 0,
		failure: nat1 || !nat20 && diff < 0,
	};
}

const updateAttackInfo = (el, { nat1, nat20, success, failure, diff } = {}) => {
	const getDiffText = () => {
		if (nat1) return game.i18n.localize('VS-AC.Nat1');
		if (nat20) return game.i18n.localize('VS-AC.Nat20');
		return signNum(diff);
	};

	el.textContent = getDiffText();
	el.classList.toggle('success', success);
	el.classList.toggle('failure', failure);
}

const fillTutorialTips = (el, { isAC, nat1, nat20, success, diff }) => {
	const title = [];
	if (nat1) title.push(game.i18n.localize('VS-AC.Tutorial.Nat1'));
	else if (nat20) title.push(game.i18n.localize('VS-AC.Tutorial.Nat20'));
	else {
		const nl = isAC ? 'AC' : 'CMD';
		title.push(game.i18n.localize(`VS-AC.Tutorial.${nl}Diff`));
		diff = Math.abs(diff);
		if (success) title.push(game.i18n.format(`VS-AC.Tutorial.Hit${nl}By`, { diff }));
		else title.push(game.i18n.format(`VS-AC.Tutorial.Miss${nl}By`, { diff }));
	}

	el.dataset.tooltip = title.join('<br>');
	el.dataset.tooltipDirection = 'UP';
}

/**
 * @param {ActionUse} usage
 */
const postActionHandler = (usage) => {
	const { item, action, shared, actor } = usage;
	if (!actor) return void console.warn('No actor');

	const userTargets = [...game.user.targets]; // Convert Set to Array
	if (userTargets.size == 0) return void console.warn('No targets');

	// Get scene from first target
	// shared.templateData.targets; // fromUuidSync(target[0].uuid)?.object.scene
	/** @type {Scene} */
	const scene = userTargets[0]?.scene;
	if (!scene) return void console.warn('No scene viewed');

	const attackerTokenId = shared.chatData.speaker.token.id;
	/** @type {TokenDocument} */
	const attackerToken = scene.tokens.get(attackerTokenId);
	const attacker = attackerToken ?? actor?.token ?? scene.tokens.find(t => t.actor.id === item.parent.id);

	if (!attacker) return void console.warn('Could not determine attacker:', item);

	const scaleTransform = (x) => sceneScaleTransform(x, attacker.parent);

	const isCMB = ['mcman', 'rcman'].includes(action.data.actionType);

	// Assume current user is the attacker
	const targets = [];
	let index = 0;
	for (const token of userTargets) {
		const tokenDoc = token.document; // scene.tokens.get(targetToken);
		if (!tokenDoc?.actor) {
			if (CFG.debug.basic) console.log('%cVS AC%c | postAttackHandler | Target has no actor:', CFG.COLORS.main, CFG.COLORS.unset, token, tokenDoc);
			continue; // No valid target
		}

		const { distance, destination, origin } = sceneTokenDistance(attacker, tokenDoc, scene);

		const attributes = tokenDoc.actor?.system.attributes,
			defenseAC = attributes.ac,
			defenseCMD = attributes.cmd;

		const targetData = {
			uuid: tokenDoc.uuid,
			// Cache AC info in case it changes
			index: index++,
			name: tokenDoc.name,
			distance,
			point: { x: scaleTransform(tokenDoc.object.center.x), y: scaleTransform(tokenDoc.object.center.y), z: tokenDoc.elevation },
			pointAdjusted: { x: destination.x, y: destination.y, z: tokenDoc.elevation },
			ac: {
				normal: defenseAC.normal.total,
				touch: defenseAC.touch.total,
				ff: defenseAC.flatFooted.total,
			},
			cmd: {
				normal: defenseCMD.total,
				ff: defenseCMD.flatFootedTotal,
			}
		};

		if (isCMB) delete targetData.ac;
		else delete targetData.cmd;

		targets.push(targetData);
	}

	const atkCenter = attacker.object.center;
	// TODO: Adjust for origin sector
	shared.chatData['flags.vsAC.origin'] = {
		point: {
			x: scaleTransform(atkCenter.x),
			y: scaleTransform(atkCenter.y),
			z: attacker.elevation
		}
	};

	shared.chatData['flags.vsAC.targets'] = targets;

	// Cache touch attack status
	const touchAttack = item.hasItemBooleanFlag('touchAttack');
	if (touchAttack) shared.chatData['flags.vsAC.defaults'] = { touch: touchAttack };

	if (CFG.debug.basic) console.log({ shared, targets, userTargets });
}

// TODO: Clarify which token is selected for comparison.
/**
 * @param {Event} event
 */
function cycleReference(event, cm) {
	// Get Chat Message instance
	/** @type HTMLElement */
	const cmEl = event.target.closest('.chat-message[data-message-id]');
	cm ??= game.messages.get(cmEl?.dataset.messageId);
	if (!cm) return;

	// Get module data
	const targetACData = cm.flags?.vsAC;
	if (targetACData === undefined) return;

	// Check if card has attacks
	const attacks = cm.getFlag('pf1', 'metadata')?.rolls?.attacks ?? [];
	if (attacks.length === 0) return;

	// Get target container
	/** @type HTMLElement */
	const targetsH = cmEl.querySelector('.vs-ac.targets');
	if (!targetsH) return;

	// Get clicked target
	/** @type HTMLElement */
	const targetEl = event.target.closest('.vs-ac.targets .target[data-uuid]');
	if (!targetEl) return;

	const index = parseInt(targetEl.dataset.index, 10);

	// Find all targets if multiple are present
	/** @type Array<HTMLElement> */
	const targets = targetsH.querySelectorAll('.target[data-uuid]'),
		sameTarget = targetEl.classList.contains('reference');
	if (!sameTarget) {
		targets.forEach(t => t.classList.remove('reference'));
		targetEl.classList.add('reference');
	}

	const targetData = targetACData.targets?.[index];
	if (!targetData) return;

	const isAC = targetData.ac !== undefined,
		cmpTypes = isAC ? ['normal', 'touch', 'ff'] : ['normal', 'ff'],
		compareOffset = sameTarget ? 1 : 0, // Progress only if same target
		cmp = targetEl.dataset.compare = cmpTypes[(cmpTypes.indexOf(targetEl.dataset.compare) + compareOffset) % cmpTypes.length];

	// Cycle compare if clicking same as previous target
	if (sameTarget) {
		targetEl.querySelectorAll('.ac [data-ac],.cmd [data-cmd]')
			.forEach(el => 	el.classList.toggle('reference', el.dataset[isAC ? 'ac' : 'cmd'] === cmp));
	}

	// Fetch cached AC/CMD
	const targetDef = isAC ? targetData.ac : targetData.cmd;
	if (targetDef === undefined) return;

	/** @type Number */
	const targetDefValue = targetDef[cmp];

	// Update AC comparisons
	cmEl.querySelectorAll('diff.vs-ac').forEach(el => {
		const attackNumber = Number(el.dataset.attack),
			atk = attacks[attackNumber],
			isCrit = el.dataset.critical === 'true',
			rollInfo = getRollInfo(!isCrit ? atk.attack : atk.critConfirm, targetDefValue);
		if (!rollInfo) return;
		rollInfo.isAC = isAC;
		updateAttackInfo(el, rollInfo);
		fillTutorialTips(el, rollInfo);
	});
}

let clickDisabled = false;
const disableClicking = () => {
	// Ignore clicks while animating. Arbitrary ignore time
	if (clickDisabled) setTimeout(_ => clickDisabled = false, 150);
	return !clickDisabled;
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
const vsACInject = (cm, [html]) => {
	const transparency = game.user.isGM ? true : game.settings.get(CFG.id, CFG.SETTINGS.transparency);
	if (!transparency) return;

	if (CFG.debug.chat) console.log('%cVS AC%c | chatMessage:', CFG.COLORS.main, CFG.COLORS.unset, { cm });
	const vsAC = cm.flags?.vsAC;
	if (!Array.isArray(vsAC?.targets) || vsAC.targets.length == 0) {
		if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | Missing module flag data:', CFG.COLORS.main, CFG.COLORS.unset, { data: vsAC, cm });
		return;
	}

	// Find first attack to anchor to
	const cma = html.querySelector('.chat-attack');
	if (!cma) {
		if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | No Attack Element found', CFG.COLORS.main, CFG.COLORS.unset);
		return;
	}

	const rangeConfig = game.settings.get(CFG.id, CFG.SETTINGS.rangeConfig);
	const tutorial = game.settings.get(CFG.id, CFG.SETTINGS.tutorial);

	const touchAttack = vsAC.defaults?.touch ?? false; // Is the attack marked as touch attack?

	const showTargets = game.settings.get(CFG.id, CFG.SETTINGS.showTargets);
	if (showTargets) {
		const origin = vsAC.origin?.point;

		// Establish basic target display container
		const targetsBox = createNode('div', null, ['vs-ac', 'targets']);
		cma.before(targetsBox);

		let index = 0, scene;
		for (const target of vsAC.targets) {
			let showAC = true, showRange = true;
			if (!game.user.isGM) {
				const reT = target.uuid.match(/Token\.(?<tokenId>\w+)/);
				if (!scene) {
					const reS = target.uuid.match(/Scene\.(?<sceneId>\w+)/);
					scene = game.scenes.get(reS?.groups.sceneId);
				}
				if (!scene) continue;
				const token = scene.tokens.get(reT?.groups.tokenId);
				target.token = token;

				// console.log({ token, actor: token.actor, visible: token.object.visible, isOwner: token.isOwner });
				if (!token?.actor) continue; // not found or has no actor
				if (!token.isOwner) showAC = false;
				if (!token.object?.visible) showRange = false;
			}

			const targetPoint = target.point;

			const targetInfo = createNode('div', null, ['target']);
			targetInfo.dataset.uuid = target.uuid;
			targetInfo.append(
				createNode('i', null, ['fas', 'fa-bullseye', 'icon']),
				createNode('span', target.name, ['vs'])
			);

			if (rangeConfig.display && showRange) {
				const mp = (n, p = 2) => Math.roundDecimals(n, p);
				const label = origin && targetPoint ? `[${mp(origin.x)}, ${mp(origin.y)}, ${mp(origin.z)}] -> [${mp(targetPoint.x)}, ${mp(targetPoint.y)}, ${mp(targetPoint.z)}]` : '';
				const rangeLabel = createNode('span', `${mp(target.distance, 1)} 📐`, ['rng']);
				rangeLabel.dataset.tooltip = label;
				rangeLabel.dataset.tooltipDirection = 'UP';
				targetInfo.append(rangeLabel);
			}

			if (showAC) {
				if (target.ac) {
					const acLabel = createNode('span', null, ['ac', 'label']);
					const normalAC = createNode('span', `${target.ac.normal}`, ['normal']),
						touchAC = createNode('span', `${target.ac.touch}`, ['touch']),
						ffAC = createNode('span', `${target.ac.ff}`, ['ff']);

					normalAC.dataset.ac = 'normal';
					touchAC.dataset.ac = 'touch';
					ffAC.dataset.ac = 'ff';

					setTooltip(normalAC, 'VS-AC.Defense.Normal');
					setTooltip(touchAC, 'VS-AC.Defense.Touch');
					setTooltip(ffAC, 'VS-AC.Defense.FF');

					const acE = !touchAttack ? normalAC : touchAC;
					acE.classList.add('reference');

					// Only the first target is reference initially
					if (index === 0) targetInfo.classList.add('reference');

					acLabel.append(normalAC, '/', touchAC, '/', ffAC);

					targetInfo.dataset.compare = !touchAttack ? 'normal' : 'touch';
					targetInfo.append(acLabel);
				}
				else if (target.cmd) {
					const cmdLabel = createNode('span', null, ['cmd', 'label']);
					const normalCMD = createNode('span', `${target.cmd.normal}`, ['normal']),
						ffCMD = createNode('span', `${target.cmd.ff}`, ['ff']);

					normalCMD.dataset.cmd = 'normal';
					ffCMD.dataset.cmd = 'ff';
					setTooltip(normalCMD, 'VS-AC.Defense.Normal');
					setTooltip(ffCMD, 'VS-AC.Defense.FF');

					normalCMD.classList.add('reference');

					// Only the first target is reference initially
					if (index === 0) targetInfo.classList.add('reference');

					cmdLabel.append(normalCMD, '/', ffCMD);

					targetInfo.dataset.compare = 'normal';
					targetInfo.append(cmdLabel);
				}
			}

			targetInfo.dataset.uuid = target.uuid;
			targetInfo.dataset.index = index++;

			targetsBox.append(targetInfo);
		}
	}

	const firstTarget = vsAC.targets[0];
	// console.log(firstTarget);
	if (!game.user.isGM && !firstTarget.token?.isOwner) {
		if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | Not Target Owner', CFG.COLORS.main, CFG.COLORS.unset);
		return;
	}

	// Attack roll to AC comparisons
	const meta = cm.getFlag('pf1', 'metadata');
	if (!meta) {
		if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | No PF1 metadata', CFG.COLORS.main, CFG.COLORS.unset);
		return;
	}

	const attacks = meta?.rolls?.attacks ?? [];
	if (attacks.length === 0) {
		if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | No Attacks', CFG.COLORS.main, CFG.COLORS.unset);
		return;
	}

	let aNo = 0;

	const isAC = firstTarget.ac !== undefined,
		targetDefenses = isAC ? firstTarget.ac : firstTarget.cmd,
		targetDef = !touchAttack ? targetDefenses.normal : targetDefenses.touch;

	html.querySelectorAll('.attack-flavor')
		?.forEach(el => {
			if (el.colSpan === 4) return; // Ignore super-header for attacks with crit-confirm

			const crit = el.classList.contains('crit-confirm');
			const attackData = attacks[aNo];
			if (!attackData) return; // Attack data doesn't match HTML elements; bail.

			const rollInfo = getRollInfo(!crit ? attackData.attack : attackData.critConfirm, targetDef);
			if (!rollInfo) {
				if (CFG.debug.chat) console.log('%cVS AC%c | ChatMessage | Could not find attack roll info', CFG.COLORS.main, CFG.COLORS.unset);
				return; // Bad data
			}

			rollInfo.isAC = isAC;

			// Insert AC comparison
			const attackNumber = aNo;
			// Increment attack number
			if (crit || !crit && !attackData.critConfirm) aNo++;

			// Shorten crit confirm label
			if (crit) el.textContent = game.i18n.localize('PF1.Confirm');

			const dEl = createNode('diff', null, ['vs-ac']);

			updateAttackInfo(dEl, rollInfo);

			el.append(dEl);

			dEl.dataset.attack = attackNumber.toString();
			dEl.dataset.critical = crit.toString();

			// Add tooltip explanations
			if (tutorial) fillTutorialTips(dEl, rollInfo);
		});
}

/**
 * Common listener for the chat log.
 * @param {Event} ev
 */
const chatLogClickHandler = (ev) => {
	// if (el.matches('.vs-ac.targets')) {
	const el = ev.target.closest('.vs-ac.targets .ac.label,.vs-ac.targets .cmd.label,.vs-ac.targets .target');
	if (!el) return;

	const msgId = el.closest('.message[data-message-id]')?.dataset.messageId;
	const cm = game.messages.get(msgId);
	if (!cm) return;

	ev.preventDefault();
	ev.stopPropagation();

	if (el.matches('.vs-ac.targets .ac.label,.vs-ac.targets .cmd.label')) {
		// Click to cycle reference

		if (!disableClicking()) return;

		cycleReference(ev);
	}
	else if (el.matches('.vs-ac.targets .target')) {
		// TODO: Hook into animation instead
		if (!disableClicking()) return;

		const token = fromUuidSync(el.dataset.uuid)
		if (token) focusToken(token, { releaseOthers: !ev.shiftKey });
	}
};

/**
 * @param {ChatLog} log
 * @param {JQuery} html
 * @param {Object} options
 */
const renderChatLogHook = (log, [html], options) => html.querySelector('ol')
	.addEventListener('click', chatLogClickHandler);

Hooks.on('renderChatLog', renderChatLogHook);

Hooks.on('renderChatMessage', vsACInject);
Hooks.on('pf1PreDisplayActionUse', postActionHandler);

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTINGS.rangeConfig, {
		default: {},
		scope: 'world',
		type: Object,
		config: false,
	});

	game.settings.registerMenu(CFG.id, CFG.SETTINGS.rangeConfig, {
		id: `${CFG.id}-range-config`,
		name: 'Range',
		// hint: 'Configure range handling.',
		label: 'Configure Range Handling',
		icon: 'fas fa-ruler-combined',
		type: RangeConfigDialog,
		restricted: true,
	});

	/*
	game.settings.register(CFG.module, CFG.SETTINGS.focus, {
		name: 'Swap scene',
		hint: 'Swap scene when target is clicked if different from current.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});
	*/

	game.settings.register(CFG.id, CFG.SETTINGS.transparency, {
		name: 'Transparency',
		hint: 'If enabled, targeting displays token name to players. If disabled, no name is shown to players.',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.showTargets, {
		name: 'Targets',
		hint: 'Display targets in chat message. If disabled, only default AC comparisons are shown.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	})

	game.settings.register(CFG.id, CFG.SETTINGS.tutorial, {
		name: 'Tutorials',
		hint: 'Add additional instructional tooltips.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.noSaves, {
		name: 'No Saves',
		hint: 'Do not display info for cards with saving throws.',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});
});
